package com.shapes;

public class Circle extends Shape{
	
	public Circle(String name, float area, float peri, int radius) {
		super(name, area, peri);
		this.radius = radius;
	}
	public int radius;

	

	public float calcArea()
	{
	
		return (float)(3.14*radius*radius);
	}
 @Override
	public float findPerimeter() {
		return (float)( 2*3.14*radius);
	}

}
