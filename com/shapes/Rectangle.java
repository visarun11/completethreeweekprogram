package com.shapes;

public class Rectangle extends Shape{
public int length;public int breadth;


public Rectangle(String name, float area, float peri, int length, int breadth) {
	super(name, area, peri);
	this.length = length;
	this.breadth = breadth;
}

@Override
public float findPerimeter() {
	return (float)(2*(length+breadth));
}

	public float calcArea() {
		
		return length*breadth;
	}
}


