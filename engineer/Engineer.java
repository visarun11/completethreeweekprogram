package engineer;

public abstract class Engineer {
public String name;
public int duration;
public String collegeName;
public int percent;
public Engineer(String name, int duration, String collegeName, int percent) {
	super();
	this.name = name;
	this.duration = duration;
	this.collegeName = collegeName;
	this.percent = percent;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public int getDuration() {
	return duration;
}

public void setDuration(int duration) {
	this.duration = duration;
}

public String getCollegeName() {
	return collegeName;
}

public void setCollegeName(String collegeName) {
	this.collegeName = collegeName;
}

public int getPercent() {
	return percent;
}

public void setPercent(int percent) {
	this.percent = percent;
}

@Override
public String toString() {
	return "Engineer [name=" + name + ", duration=" + duration + ", collegeName=" + collegeName + ", percent=" + percent
			+ "]";
}
abstract void showSpecl();
abstract void showProject();

}
