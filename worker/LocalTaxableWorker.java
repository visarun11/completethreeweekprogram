package worker;

public class LocalTaxableWorker extends StateTaxableWorker{
public String cityname;public float citytaxrate;

public LocalTaxableWorker(String name, float federaltaxrate, float PayRate, String statename, float statetaxrate,
		String cityname, float citytaxrate) {
	super(name, federaltaxrate, PayRate, statename, statetaxrate);
	this.cityname = cityname;
	this.citytaxrate = citytaxrate;
}

public float getCitytaxrate() {
	return citytaxrate;
}

public void setCitytaxrate(float citytaxrate) {
	this.citytaxrate = citytaxrate;
}

@Override
public String toString() {
	return "LocalTaxableWorker [cityname=" + cityname + ", citytaxrate=" + citytaxrate + ", statename=" + statename
			+ ", statetaxrate=" + statetaxrate + ", name=" + name + ", federaltaxrate=" + federaltaxrate + ", PayRate="
			+ PayRate + ", hrs=" + hrs + ", grossPayamount=" + grossPayamount + "]";
}
public double taxWithheld(double grossPayamount)
{   double taxheld=grossPayamount*federaltaxrate;
	double statetaxheld=grossPayamount*statetaxrate;
	double staxheld = statetaxheld+taxheld;
	double localtaxheld=grossPayamount*citytaxrate;
	double ltaxheld=(double)staxheld+localtaxheld;
	return ltaxheld;
}
}
