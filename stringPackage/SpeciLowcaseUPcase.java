package stringPackage;
import java.util.Scanner;
public class SpeciLowcaseUPcase {

	public static void main(String[] args) {
		Scanner scn=new Scanner(System.in);   
		System.out.println("Enter a string:");   
		String s=scn.nextLine();  
		char ch[]=s.toCharArray();  
		for (int i = 0; i < ch.length; i++) {
		if (ch[i]>=65 && ch[i]<=90) {
			System.out.println("Character is a capital letter");}
		else if (ch[i]>=97 && ch[i]<=122)
			{System.out.println("Character is a small letter");}
		else if (ch[i]>=48 && ch[i]<=57)
			{System.out.println("Character is a digit");}
		else if ((ch[i]>0 && ch[i]<=47)||(ch[i]>=58 && ch[i]<=64)||
	               (ch[i]>=91 && ch[i]<=96)||(ch[i]>=123 && ch[i]<=127))
			{System.out.println("Character is a special symbol");}
		
	}

}}
