package excephandpackage;

public class Number {

	public static void main(String[] args) {
		int i=200;
		
		try {
			if(i>100)
				throw  new MyException();
			else 
				System.out.println(i);
			
		}
		catch(MyException e){
			System.out.println(i);
		}

	}

}
