package excephandpackage;

public class AgeEx {
	public void doThis(int age) throws AgeException
	{
		if(age<0)
			throw new AgeException("not born");
	}

	public static void main(String[] args) {
		AgeEx m =new AgeEx();
		try {
			m.doThis(-45);
		}catch(AgeException e) {
			e.printStackTrace();
		}

	}

}
