package methodPackage;
public class Students
{
	private int rollNo;
	private String studName;
	private Address addr;
	
	
public Students(int rollNo, String studName, Address addr) {
		super();
		this.rollNo = rollNo;
		this.studName = studName;
		this.addr = addr;
	}

public String toString() {
	return "Students [rollNo=" + rollNo + ", studName=" + studName + ", addr=" + addr.stNo + addr.streetName +addr.city + "]";
}
class Address{
	int stNo;
	String streetName;
	String city;
	public Address(int stNo,String streetName,String city) {
		super();
		this.stNo=stNo;
		this.streetName=streetName;
		this.city=city;
	}
}


public static void main(String[] args) {
	 
		
		Students s2=new Students(2,"bevina",new Address(23,"budha","tanjore"));
		Students s3=new Students(2,"bevina",new Address(23,"budha","tanjore"));
		Students s4=new Students(2,"bevina",new Address(23,"budha","tanjore"));
		Students s5=new Students(2,"bevina",new Address(23,"budha","tanjore"));
		System.out.println(s3);
		System.out.println(s2);
	}

}
