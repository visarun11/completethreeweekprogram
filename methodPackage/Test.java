package methodPackage;

public class Test {

	
		   	public static void main(String[] args) {
			Employee e = new Employee(1,"visa lakshi",565656);
			//creates a object - calls a constructor 
			System.out.println(e.empId);
			System.out.println(e.empName);
			System.out.println(e.empSalary);
			Employee e1 = new Employee(2,"Bevina",565656);
			//creates a object - calls a constructor 
			System.out.println(e1.empId);
			System.out.println(e1.empName);
			System.out.println(e1.empSalary);
			Employee e2 = new Employee();
			//creates a object - calls a constructor 
			System.out.println(e2.empId);
			System.out.println(e2.empName);
			System.out.println(e2.empSalary);
			Employee e3=new Employee(3);
			System.out.println(e3.empId);
			Employee e4=new Employee(4,"naveena");
			System.out.println(e4.empId);
			System.out.println(e4.empName);
		   	
		   	}
		   	
		
	}


