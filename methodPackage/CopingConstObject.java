package methodPackage;

public class CopingConstObject {
       public int id;public String name;
       public CopingConstObject(int id,String name)
       {
    	   this.id=id;
    	   this.name=name;
       }
       public CopingConstObject(CopingConstObject e1)
       { 
    	      System.out.println("Copy constructor called");
    	      id = e1.id;
    	      name = e1.name;
    	    }      
    	   public String toString() {
	        return ( id +" " + " " + name );
	    }    
       
	public static void main(String[] args) {
		
	
		    
		    	CopingConstObject e1=new CopingConstObject(12,"neha");
		    	CopingConstObject e2=new CopingConstObject(e1);
		        System.out.println(e1);
		    }
		

	}


